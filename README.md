# README #

**Server Requirements**
+ see Laravel 5.7 server requirements

**Instalation:**
+ `composer update`
+ copy settings from *.env.example* to *.env*
+ create database
+ configure *.env*
+ `php artisan key:generate`
+ `php artisan migrate`

**Preferably but not necessarily seeding database with test users and articles**
+ `php artisan db:seed`